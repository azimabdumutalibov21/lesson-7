function stopwatch(elem){
    var time = 0;
    var offset;
    var interval;


    function lapOn(){
        var lapTime = lap_box.appendChild(document.createElement('li'))
        lapTime.innerHTML=elem.textContent;
        lapTime.classList = 'lapItem'
    }


    function lapOff(){
        return;
    }

    function updata(){
        if(this.isOn){
            time+=delta();
        }
        elem.textContent=timeFormatter(time);
    }
    function delta(){
        var now = Data.now();
        var timePassed = now - offset;
        offset = now ;
        return timePassed
    }
    function timeFormatter(time){
        time =new Data(time);

        var minutes = time.getMinutes().toString();
        var seconds = time.getSeconds().toString()
        var milliseconds =time.getMilliseconds().toString();
       
        
        if(minutes.length<2){
            minutes ='0'+minutes;
        }
        if(seconds.length<2){
            seconds ='0'+seconds;
        }
        while(milliseconds.length<3){
            milliseconds='0'+milliseconds;
        }
        var result = minutes+':'+seconds+'.'+milliseconds;
        return result;
    }
    this.start=function(){
        interval=setInterval(updata.bind(this),1);
        offset = Data.now;
        this.isOn=true;
    };
    this.stop=function(){
        clearInterval(interval);
        interval=null;
        this.isOn=false;
    };
    this.reset=function(){
        time = 0;
        lap_box.innerHTML= '';
        interval=null;
        this.isOn=false;
        updata();
    };

    this.lapOn=function(){
        lapOn();
    }

    this.lapOff=function(){
        lapOff();
    }
    this.isOn=false;
}